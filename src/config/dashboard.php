<?php

return [

    /**
     * This flag enables the example entities to be shown in the dashboard for demonstration purposes.
     * To turn this off, simply change this value to false
     */
    'example_mode' => true
];