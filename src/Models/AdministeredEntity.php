<?php namespace Polylab\Dashboard\Models;

use Illuminate\Database\Eloquent\Model;
use Polylab\Dashboard\Traits\IsAdministered;
use Polylab\Entities\Traits\IsEntity;


/**
 * Class AdministeredEntity
 * @package Polylab\Dashboard\Models
 *
 * This is an example implementation of an entity that can be managed and administered inside the
 * Dashboard. To get an entity up and running this should be all it takes, and you can add functionality
 * as you see fit in your concrete implementation.
 */
class AdministeredEntity extends Model
{
    /**
     * Including the IsEntity trait makes it so the model/entity inherits methods and concepts
     * used by other modules to interact with an entity.
     *
     * Including the IsAdministered trait makes it so the model/entity implements the methods and
     * permissions functionality needed inside the Dashboard.
     *
     * These different methods can of course be used with the entity throughout your application.
     */
    use IsAdministered;
    use IsEntity;

    /**
     * @var array
     *
     * This provides a way to set which attributes should be viewable inside more protected areas, such
     * as an Admin panel or dashboard.
     * Retrieve a key value array of the attributes from the model using $this->getAdminAttributes();
     */
    protected $adminAttributes = [
        'id',
        'name',
        'color',
        'size'
    ];

    /**
     * @var array
     *
     * This provides a way to set which attributes should be used inside admin forms, such
     * as when creating or editing an entity.
     * Retrieve a key value array of the attributes from the model using $this->getFormAttributes();
     */
    protected $formAttributes = [
        'name',
        'color',
        'size'
    ];

    /**
     * @var array
     *
     * This is core laravel functionality, you can find out more by looking into the Eloquent documents at laravel.com
     */
    protected $fillable = [
        'name',
        'color',
        'size'
    ];
}