<?php namespace Polylab\Dashboard\Models;

use \Polylab\Entities\Models\EntityTypes;

/**
 * Class AdministeredEntities
 * @package Polylab\Dashboard\Models
 *
 * Aggregates all entities that can be administered
 */
class AdministeredEntities extends EntityTypes
{
    public function getEntities() {
        $oEntities = $this->app->tagged('entities');
        $returnEntities = [];

        foreach($oEntities as $oEntity) {
            if($oEntity->isAdministered) {
                $returnEntities[] = $oEntity;
            }
        }

        return $returnEntities;
    }
}