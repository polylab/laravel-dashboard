<?php namespace Polylab\Dashboard\Traits;

/**
 * Class IsAdministered
 * @package Polylab\Dashboard\Traits
 *
 * Base functionality for a permissions system inside the admin section.
 */
trait IsAdministered
{
    public $isAdministered = true;

    public function getEditPermissionName() {
        return 'edit-'.$this->getModelNamePlural();
    }

    public function getDeletePermissionName() {
        return 'delete-'.$this->getModelNamePlural();
    }

    public function getCreatePermissionName() {
        return 'create-'.$this->getModelNamePlural();
    }

    /**
     * @return array
     *
     * Returns a key value array of attributes intended to be visible in an admin section,
     * as determined by the $this->adminAttributes array.
     */
    public function getAdminAttributes() {
        $aAttributes = [];
        $aReturn = [];

        if(is_array($this->adminAttributes)) $aAttributes = $this->adminAttributes;

        foreach($aAttributes as $vAttributeName) {
            if($vAttributeName === '*') continue;

            $aReturn[$vAttributeName] = $this->{$vAttributeName};
        }

        return $aReturn;
    }

    /**
     * @return array
     *
     * Returns a key value array of attributes intended to be used inside admin forms,
     * such as creating or editing an entity.
     */
    public function getFormAttributes() {
        $aAttributes = [];
        $aReturn = [];

        if(is_array($this->formAttributes)) $aAttributes = $this->formAttributes;

        foreach($aAttributes as $vAttributeName) {
            if($vAttributeName === '*') continue;

            $aReturn[$vAttributeName] = $this->{$vAttributeName};
        }

        return $aReturn;
    }

    /**
     * @param $vTemplate
     * @return string
     *
     * Gets an admin template view path from the model, if one is not set use this modules default templates.
     *
     * An example would be a list template to show a list of this particular
     * type of model. Inside your concrete implementation of the model you would
     * specify a protected variable called listAdminTemplate, with the view path, eg: 'mytheme.myentity.list'
     * Then when the model is used by other parts of your application you can simply
     * call $yourEntity->getAdminTemplate('list') to get the list template path.
     */
    public function getAdminTemplate($vTemplate) {
        $templateProperty = $vTemplate.'AdminTemplate';
        return isset($this->{$templateProperty})
            ? $this->{$templateProperty}
            : 'dashboard::entities.default.' . $vTemplate;
    }
}