<?php namespace Polylab\Dashboard\Traits;

/**
 * Class CanCheckPermissions
 * @package Polylab\Dashboard\Traits
 *
 * Class can check and check for permissions
 */
trait CanCheckPermissions
{
    public $hasPermissions = true;

    public function getPermissionValue($vPermissionString) {
        $this->getData($vPermissionString);
    }
}