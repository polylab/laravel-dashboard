<?php namespace Polylab\Dashboard\Controllers;

//Framework
use Gate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

//Polylab
use Polylab\Dashboard\Models\AdministeredEntities;

class AdminController extends Controller {

    public function __construct(AdministeredEntities $oEntityTypes) {
        $this->entityTypes = $oEntityTypes;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('dashboard::index', ['entities' => $this->entityTypes->getEntities()]);
    }

    /**
     * @param $vEntity
     * @param int $perPage
     * @return \Illuminate\View\View
     */
    public function listEntity($vEntity, $perPage = 15)
    {
        $oEntity = $this->entityTypes->getEntity($vEntity);
        $oEntityCollection = $oEntity->paginate($perPage);

        return view($oEntity->getAdminTemplate('list'), ['entityCollection' => $oEntityCollection, 'modelInstance' => $oEntity, 'entities' => $this->entityTypes->getEntities()]);
    }

    /**
     * @param $vEntity
     * @param $vEntityId
     * @return \Illuminate\View\View
     */
    public function viewEntity($vEntity, $vEntityId)
    {
        $oEntity = $this->entityTypes->getEntity($vEntity);
        $oEntity = $oEntity->find($vEntityId);

        return view($oEntity->getAdminTemplate('view'), ['entity' => $oEntity, 'entities' => $this->entityTypes->getEntities()]);
    }

    /**
     * @param $vEntity
     * @return \Illuminate\View\View
     */
    public function createEntity($vEntity)
    {
        $oEntity = $this->entityTypes->getEntity($vEntity);

        return view($oEntity->getAdminTemplate('create'), ['modelInstance' => $oEntity, 'entities' => $this->entityTypes->getEntities()]);
    }

    /**
     * @param $vEntity
     * @param $vEntityId
     * @return \Illuminate\View\View
     */
    public function editEntity($vEntity, $vEntityId)
    {
        $oEntity = $this->entityTypes->getEntity($vEntity)
                                ->find($vEntityId);

        return view($oEntity->getAdminTemplate('edit'), ['modelInstance' => $oEntity, 'entities' => $this->entityTypes->getEntities()]);
    }

    /**
     * @param $vEntity
     * @param Request $oRequest
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeEntity($vEntity, Request $oRequest)
    {
        /** @var $oEntity \Illuminate\Database\Eloquent\Model */
        $oEntity = $this->entityTypes->getEntity($vEntity);

        if($oRequest->input('id')) {
            $oEntity = $oEntity->find($oRequest->input('id'));
        }

        $oEntity->fill($oRequest->all());
        $oEntity->save();

        return redirect('/admin/' . $vEntity . '/list');
    }

}