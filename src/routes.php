<?php

// Authentication routes...
Route::group(['middleware' => 'web'], function () {
    Route::get('admin', 'AdminController@index');
    Route::get('admin/{entity}/list', 'AdminController@listEntity');
    Route::get('admin/{entity}/create', 'AdminController@createEntity');
    Route::post('admin/{entity}/create', 'AdminController@storeEntity');
    Route::get('admin/{entity}/view/{id}', 'AdminController@viewEntity');
    Route::get('admin/{entity}/edit/{id}', 'AdminController@editEntity');
    Route::post('admin/{entity}/edit', 'AdminController@storeEntity');
});