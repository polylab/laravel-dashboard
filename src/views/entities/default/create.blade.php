@extends('dashboard::dashapp')
@section('body-class', ' entity-create')
@section('page-title', $modelInstance->getModelNamePlural())

@section('content')
<div class="entity-create-container {{strtolower($modelInstance->getModelName())}}-create">
    <div class="inner-container">
        <h4 class="title">Create a new {{$modelInstance->getModelName()}}</h4>
        <div class="entity-form">
            <form class="delete-form top entity" role="form" method="POST" action="/admin/{{$modelInstance->getModelName()}}/create">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="attributes">
                    @foreach($modelInstance->getFormAttributes() as $name => $value)
                        <div class="attribute {{ strtolower($name) }}-attribute">
                            <div class="name">{{ ucfirst($name)}}</div>
                            <input type="text" name="{{$name}}" class="value" value="{{$value}}" />
                        </div>
                    @endforeach
                </div>
                <button type="submit" class="button cta create-entity-button">Create New {{$modelInstance->getModelName()}}</button>
            </form>
       </div>
    </div>
</div>
@endsection