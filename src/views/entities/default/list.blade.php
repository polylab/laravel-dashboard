@extends('dashboard::dashapp')
@section('body-class', ' entity-list')
@section('page-title', $modelInstance->getModelNamePlural())

@section('content')
<div class="entity-list-container {{strtolower($modelInstance->getModelName())}}-list">
    <a class="button cta create-entity-button" href="/admin/{{$modelInstance->getModelName()}}/create">Create New {{$modelInstance->getModelName()}}</a>
    <div class="inner-container">
        @if(count($entityCollection))
        <h4 class="title">{{$modelInstance->getModelNamePlural()}}</h4>
        @endif
        <div class="entities">
            @foreach($entityCollection as $entity)
            <div class="entity">
                <div class="attributes">
                    @foreach($entity->getAdminAttributes() as $name => $value)
                        <div class="attribute {{ strtolower($name) }}-attribute">
                            <div class="name">{{ ucfirst($name)}}</div>
                            <div class="value">{{$value ? $value : "Not Set"}}</div>
                        </div>
                    @endforeach
                </div>
                <a class="button view-entity" href="/admin/{{$entity->getModelName()}}/view/{{$entity->id}}">View</a>
                <a class="button edit-entity" href="/admin/{{$entity->getModelName()}}/edit/{{$entity->id}}">Edit</a>
            </div>
            @endforeach
            @if($entityCollection->perPage() < $entityCollection->total())
                <div class="pagination">
                    @if($entityCollection->previousPageUrl())
                    <a class="previous button" href="{{$entityCollection->previousPageUrl()}}">Previous</a>
                    @endif
                    @if($entityCollection->nextPageUrl())
                    <a class="next button" href="{{$entityCollection->nextPageUrl()}}">Next</a>
                    @endif
                </div>
            @endif
        </div>
        @if(!$entityCollection->count())
            <div class="no-results-container">
                <h3 class="heading">There doesn't seem to be any {{$modelInstance->getModelNamePlural()}}</h3>
                <a class="create-button button" href="/admin/{{$modelInstance->getModelName()}}/create/">Create {{$modelInstance->getModelName()}}</a>
            </div>
        @endif
    </div>
</div>
@endsection