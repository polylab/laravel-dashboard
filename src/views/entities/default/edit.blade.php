@extends('dashboard::dashapp')
@section('body-class', ' entity-create')
@section('page-title', $modelInstance->getModelNamePlural())

@section('content')
<div class="entity-edit-container {{strtolower($modelInstance->getModelName())}}-edit">
    <div class="inner-container">
        <h4 class="title">Edit {{$modelInstance->getModelName()}}</h4>
        <div class="entity-form">
            <form class="delete-form top entity" role="form" method="POST" action="/admin/{{$modelInstance->getModelName()}}/edit">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <input type="hidden" name="id" value="{{$modelInstance->id}}" />
                <div class="attributes">
                    @foreach($modelInstance->getFormAttributes() as $name => $value)
                        <div class="attribute {{ strtolower($name) }}-attribute">
                            <div class="name">{{ ucfirst($name)}}</div>
                            <input type="text" name="{{$name}}" class="value" value="{{$value}}" />
                        </div>
                    @endforeach
                </div>
                <button type="submit" class="button cta edit-entity-button">Save Changes</button>
            </form>
       </div>
    </div>
</div>
@endsection