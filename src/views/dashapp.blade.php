<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('page-title') - Admin @yield('page-title-after')</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link href="/css/app.css" rel="stylesheet">

	@yield('page-css')
	@yield('header-scripts')
</head>
<body class="dashboard @yield('body-class')">
	<nav class="header-menu">
		<div class="component-menu menu-container" id="header-menu">
			<div class="mobile-menu-button" id="mobile-menu-button"></div>
			<a class="menu-item has-icon home" href="/admin">Dashboard</a>
			@foreach($entities as $entity)
			    <a class="menu-item {{$entity->getModelName()}}" href="/admin/{{$entity->getModelName()}}/list">{{ucwords($entity->getModelNamePlural())}}</a>
			@endforeach
            <a class="menu-item right" href="/auth/logout">Logout</a>
            <a class="menu-item right" href="/account">{{ Auth::user()->name }} <span class="caret"></span></a>
		</div>
	</nav>
	<div class="page-container">
		@if ($errors)
			<div class="message-container">
				@foreach($errors as $error)
					<div class="message error">{{$message}}</div>
				@endforeach
			</div>
		@endif

		@if (array_key_exists('sidebar-content', View::getSections()))
		<div class="sidebar-container">
			@yield('sidebar-content')
		</div>
		@endif
		<div class="content-main-container">
			@yield('content')
		</div>
	</div>
	<div class="footer-container">
		@yield('footer-extra')
		<div class="footer-content">
			<div class="copyright">Dashboard by <a href="http://polylab.co/">Polylab</a> ❤</div>
		</div>
	</div>
	@yield('footer-scripts')
</body>
</html>
