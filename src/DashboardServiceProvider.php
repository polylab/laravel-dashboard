<?php namespace Polylab\Dashboard;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider;

class DashboardServiceProvider extends AuthServiceProvider
{
    public function register()
    {
        $this->app->router->group(['namespace' => 'Polylab\Dashboard\Controllers'], function(){
            require __DIR__.'/routes.php';
        });

        if(config('dashboard.example_mode')){
            $this->app->bind('AdministeredEntity', function () {
                return new Models\AdministeredEntity();
            });

            //Group entities into an Service Container group, refer to Entities\Models\Entities for more information on how this is set up
            $this->app->tag(['AdministeredEntity'], 'entities');

            //Make binding that will return all entities from the tagged group
            $this->app->bind('AdministeredEntities', function ($oApp) {
                return new Models\AdministeredEntities($oApp);
            });
        }
    }

    public function boot(Gate $gate)
    {
        //Define Admin Permisions and Access Control
        $this->registerPolicies($gate);

        //Check for specific permissions
        $gate->define('access-dashboard', function ($user) {
            return $user->getPermissionValue('access-dashboard');
        });

        //If super admin, bypass the rest of the validation
        $gate->before(function ($user) {
            if ($user->getPermissionValue('super-user')) {
                return true;
            }
        });

        $this->_registerViews();
        $this->_registerPublishables();
    }

    private function _registerViews() {
        $this->loadViewsFrom(__DIR__.'/views', 'dashboard');
    }

    private function _registerPublishables() {
        $this->publishes([
            realpath(__DIR__.'/migrations') => $this->app->databasePath().'/migrations',
            realpath(__DIR__.'/assets/sass') => base_path('resources/assets/sass/vendor/polylab'),
            realpath(__DIR__.'/config/dashboard.php') => config_path('dashboard.php')
        ]);
    }
}