<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdministeredentityTable extends Migration
{
    public function up()
    {
        if(Schema::hasTable('administered_entities')) return;

        Schema::create('administered_entities', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('color');
            $table->string('size');
            $table->timestamps();
        });
    }
}
